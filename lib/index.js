var VDataFilter = {
  install: function(Vue, options) {
    /**
     * data filter
     * filter to convert data from/to
     * usage:
     * 1 | data({ from: 'gb', to: 'bytes' })
     * 1073741824 | data({ from: 'bytes', to: 'gb' })
     */
    Vue.filter('data', (value, options = {}) => {
      const defaults = { to: '', from: 'bytes', decimals: 2 }
      value = Number(value)
      options = Object.assign({}, defaults, options)

      const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

      const fromIndex = sizes.map(o => o.toLowerCase()).indexOf(options.from.toLowerCase())
      let nextIndex = value > 1024 ? (fromIndex + 1) : fromIndex
      if (!options.to) {
        let nextSize = value / 1024
        while (nextSize > 1024) {
          nextIndex += 1
          nextSize = nextSize / 1024
        }
      }

      const toIndex = options.to ? sizes.map(o => o.toLowerCase()).indexOf(options.to.toLowerCase()) : nextIndex

      const format = (val, sizeIndex = toIndex) => {
        if (val === 1024) {
          if (options.onlyValue) { return 1 }
          return `1 ${sizes[sizeIndex + 1]}`
        }
        if ((val % 1) === 0) {
          if (options.onlyValue) { return val }
          return `${val} ${sizes[sizeIndex]}`
        }

        if (options.onlyValue) { return val.toFixed(options.decimals) }
        return `${val.toFixed(options.decimals)} ${sizes[sizeIndex]}`
      }

      if (fromIndex === toIndex) {
        return format(value)
      }

      if (toIndex === -1) {
        let i
        for (i = (fromIndex + 1); i < sizes.length; i++) {
          const rate = Math.pow(1024, i - fromIndex)
          const newVal = value / rate

          if (newVal < 1) {
            return format(newVal * 1024, i - 1)
          } else if (newVal < 1024) {
            return format(newVal, i)
          }
        }
      }

      if (fromIndex > toIndex) {
        const rate = Math.pow(1024, fromIndex - toIndex)
        return format(value * rate)
      }

      if (fromIndex < toIndex) {
        const rate = Math.pow(1024, toIndex - fromIndex)
        return format(value / rate)
      }

      return ''
    })
  }
}

export default VDataFilter;

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(VDataFilter);
  window.VDataFilter = VDataFilter;
}
