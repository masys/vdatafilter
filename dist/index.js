'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var VDataFilter = {
  install: function install(Vue, options) {
    /**
     * data filter
     * filter to convert data from/to
     * usage:
     * 1 | data({ from: 'gb', to: 'bytes' })
     * 1073741824 | data({ from: 'bytes', to: 'gb' })
     */
    Vue.filter('data', function (value) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      var defaults = { to: '', from: 'bytes', decimals: 2 };
      value = Number(value);
      options = Object.assign({}, defaults, options);

      var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

      var fromIndex = sizes.map(function (o) {
        return o.toLowerCase();
      }).indexOf(options.from.toLowerCase());
      var nextIndex = value > 1024 ? fromIndex + 1 : fromIndex;
      if (!options.to) {
        var nextSize = value / 1024;
        while (nextSize > 1024) {
          nextIndex += 1;
          nextSize = nextSize / 1024;
        }
      }

      var toIndex = options.to ? sizes.map(function (o) {
        return o.toLowerCase();
      }).indexOf(options.to.toLowerCase()) : nextIndex;

      var format = function format(val) {
        var sizeIndex = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : toIndex;

        if (val === 1024) {
          if (options.onlyValue) {
            return 1;
          }
          return '1 ' + sizes[sizeIndex + 1];
        }
        if (val % 1 === 0) {
          if (options.onlyValue) {
            return val;
          }
          return val + ' ' + sizes[sizeIndex];
        }

        if (options.onlyValue) {
          return val.toFixed(options.decimals);
        }
        return val.toFixed(options.decimals) + ' ' + sizes[sizeIndex];
      };

      if (fromIndex === toIndex) {
        return format(value);
      }

      if (toIndex === -1) {
        var i = void 0;
        for (i = fromIndex + 1; i < sizes.length; i++) {
          var rate = Math.pow(1024, i - fromIndex);
          var newVal = value / rate;

          if (newVal < 1) {
            return format(newVal * 1024, i - 1);
          } else if (newVal < 1024) {
            return format(newVal, i);
          }
        }
      }

      if (fromIndex > toIndex) {
        var _rate = Math.pow(1024, fromIndex - toIndex);
        return format(value * _rate);
      }

      if (fromIndex < toIndex) {
        var _rate2 = Math.pow(1024, toIndex - fromIndex);
        return format(value / _rate2);
      }

      return '';
    });
  }
};

exports.default = VDataFilter;


if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(VDataFilter);
  window.VDataFilter = VDataFilter;
}